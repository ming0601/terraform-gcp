# From https://gmusumeci.medium.com/getting-started-with-terraform-and-google-cloud-platform-gcp-deploying-vms-in-a-private-only-f9ab61fa7d15
# VMs in private-only single region with LB and Unmanaged Instance Groups

# Step 1: The Provider & Variables
terraform {
  required_version = "=0.14.7"
}

provider "google" {
  project = var.app_project
  credentials = file(var.gcp_auth_file)
  region = var.gcp_region_1
  zone = var.gcp_zone_1
}

provider "google-beta" {
  project = var.app_project
  credentials = file(var.gcp_auth_file)
  region = var.gcp_region_1
  zone = var.gcp_zone_1
}

# Step 2: Network
# Step 3: NAT Router
module "vpc_network" {
  source = "../../modules/vpc-network"
  app_name = var.app_name
  app_project = var.app_project
  gcp_region_1 = var.gcp_region_1
  private_subnet_cidr_1 = var.private_subnet_cidr_1
}

# Step 4: Firewall rules
module "network_firewall" {
  source = "../../modules/network-firewall"
  app_name = var.app_name
  network_name = module.vpc_network.vpc_network_name
}

# Step 5: Create GCP VMs
module "private_vm_instance_1" {
  source = "../../modules/vm-private-instance"
  app_name = "${var.app_name}-vm1"
  app_hostname = "${var.app_name}-vm1.${var.app_domain}"
  gcp_zone = var.gcp_zone_1
  vpc_name = module.vpc_network.vpc_network_name
  private_subnet_name = module.vpc_network.vpc_private_subnet_name
  metadata_startup_script = "sudo apt-get update && sudo apt-get install -yq build-essential apache2"
}

module "private_vm_instance_2" {
  source = "../../modules/vm-private-instance"
  app_name = "${var.app_name}-vm2"
  app_hostname = "${var.app_name}-vm2.${var.app_domain}"
  gcp_zone = var.gcp_zone_1
  vpc_name = module.vpc_network.vpc_network_name
  private_subnet_name = module.vpc_network.vpc_private_subnet_name
  metadata_startup_script = <<-EOT
    #!bin/bash
    sudo apt-get update && sudo apt-get install apache2 -y &&
    echo '<!doctype html><html><body><h1>Hello Terraform GCE UIG Project!</h1></body></html>' | sudo tee /var/www/html/index.html
    EOT
}

# Step 6: Create the Load Balancer
# Load Balancer with unmanaged instance group
module "lb_unmanaged" {
  source = "../../modules/load-balancers/unmanaged"
  app_name = var.app_name
  app_project = var.app_project
  gcp_zone = var.gcp_zone_1
  mig_instance_self_links_list = [ module.private_vm_instance_1.private_vm_instance_self_link, module.private_vm_instance_2.private_vm_instance_self_link ]
}