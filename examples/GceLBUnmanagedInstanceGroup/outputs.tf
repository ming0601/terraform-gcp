output "load-balancer-ip-address" {
  value = module.lb_unmanaged.load-balancer-ip-address
}

output "nat_ip_address" {
  value = module.vpc_network.nat_ip_address
}

output "private_vm_instance_1_self_link" {
  value = module.private_vm_instance_1.private_vm_instance_self_link
}

output "private_vm_instance_2_self_link" {
  value = module.private_vm_instance_2.private_vm_instance_self_link
}

output "web-name-1" {
  value = module.private_vm_instance_1.web-name
}

output "web-name-2" {
  value = module.private_vm_instance_2.web-name
}

output "web-1-internal-ip" {
  value = module.private_vm_instance_1.web-internal-ip
}

output "web-2-internal-ip" {
  value = module.private_vm_instance_2.web-internal-ip
}