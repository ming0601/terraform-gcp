# From Gruntwork.io: https://github.com/gruntwork-io/terraform-google-gke/blob/master/examples/gke-private-cluster/main.tf
# ---------------------------------------------------------------------------------------------------------------------
# DEPLOY A GKE PRIVATE CLUSTER IN GOOGLE CLOUD PLATFORM
# This is an example of how to use the gke-cluster module to deploy a private Kubernetes cluster in GCP
# ---------------------------------------------------------------------------------------------------------------------

terraform {
  required_version = "=0.14.7"
}

# Step 1: Providers
provider "google" {
  credentials = file(var.gcp_auth_file)
  project = var.project
  region = var.region
}

provider "google-beta" {
  credentials = file(var.gcp_auth_file)
  project = var.project
  region = var.region
}

# Step 2: Network
# Step 3: NAT Router
module "vpc_network" {
  source = "../../modules/vpc-network"
  app_name = var.cluster_name
  app_project = var.project
  gcp_region_1 = var.region
  private_subnet_cidr_1 = var.vpc_cidr_block
}

# Step 5: GKE master cluster
module "gke_cluster" {
  source = "../../modules/gke-cluster"
  depends_on = [ module.vpc_network ]

  name = var.cluster_name
  project = var.project
  location = var.location
  network = module.vpc_network.vpc_network_self_link

  # We're deploying the cluster in the private subnetwork
  subnetwork = module.vpc_network.vpc_private_subnet_self_link

  # When creating a private cluster, the 'master_ipv4_cidr_block' has to be defined and the size must be /28
  master_ipv4_cidr_block = var.master_ipv4_cidr_block

  # This setting will make the cluster private
  enable_private_nodes = "true"

  # To make testing easier, we keep the public endpoint available: disable_public_endpoint = "false"
  # In production, we highly recommend restricting access to only within the network boundary,
  # requiring your users to use a bastion host or VPN.
  disable_public_endpoint = "true"

  # With a private cluster, it is highly recommended to restrict access to the cluster master
  # However, for testing purposes we will allow all inbound traffic: "0.0.0.0/0".
  master_authorized_networks_config = [
    {
      cidr_blocks = [
        {
          cidr_block = module.vpc_network.vpc_private_subnet_ip_cidr_range
          display_name = "${module.vpc_network.vpc_private_subnet_name}-IP-Range"
        }
      ]
    }
  ]

  # setting this value to null lets Google derive values that actually work.
  cluster_secondary_range_name = null

  enable_vertical_pod_autoscaling = var.enable_vertical_pod_autoscaling

  resource_labels = {
    environment = "testing"
  }
}

# Step 6: Create a GKE service account
module "gke_service_account" {
  source = "../../modules/gke-service-account"
  name = var.cluster_service_account_name
  project = var.project
  description = var.cluster_service_account_description
}

# Step 7: Create private node pools
resource "google_container_node_pool" "node_pool" {
  provider = google-beta
  name = "private-pool"
  project = var.project
  location = var.location
  cluster = module.gke_cluster.name

  initial_node_count = "1"

  autoscaling {
    max_node_count = "5"
    min_node_count = "1"
  }

  management {
    auto_repair = "true"
    auto_upgrade = "true"
  }

  node_config {
    image_type = "COS"
    machine_type = "n1-standard-1"

    labels = {
      private-pool-example = "true"
    }

    # Add a private tag to the instances. See the network access tier table for full details:
    # https://github.com/gruntwork-io/terraform-google-network/tree/master/modules/vpc-network#access-tier
    tags = [ module.vpc_network.vpc_network_name, "private-pool-example" ]

    disk_size_gb = "30"
    disk_type = "pd-standard"
    preemptible = false

    service_account = module.gke_service_account.email

    oauth_scopes = [ "https://www.googleapis.com/auth/cloud-platform" ]
  }

  lifecycle {
    ignore_changes = [ initial_node_count ]
  }

  timeouts {
    create = "30m"
    update = "30m"
    delete = "30m"
  }
}

# Step 8: Create a SSH Firewall rule
resource "google_compute_firewall" "fw_allow_ssh_from_iap_to_instances" {
  name = "${var.cluster_name}-allow-ssh-from-iap"
  network = module.vpc_network.vpc_network_name
  direction = "INGRESS"

  allow {
    protocol = "tcp"
    ports = [ "22" ]
  }
  # https://cloud.google.com/iap/docs/using-tcp-forwarding#before_you_begin
  # This is the netblock needed to forward to the instances
  source_ranges = [ "35.235.240.0/20" ]

  # Allow all ips for testing
//  source_ranges = [ "0.0.0.0/0" ]

  target_tags = [ "ssh-to-bastion-host" ]
}

# Step 9: Create a Bastion Host
resource "google_compute_instance" "bastion_host" {
  project = var.project
  name = "default-bastion-host"
  machine_type = "f1-micro"
  zone = var.bastion_host_zone
  allow_stopping_for_update = true
  tags = [ "ssh-to-bastion-host" ]

  metadata_startup_script = "sudo apt-get update && sudo apt-get install -y kubectl"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  network_interface {
    network = module.vpc_network.vpc_network_self_link
    subnetwork = module.vpc_network.vpc_private_subnet_self_link
//
//    access_config {
//      // Ephemeral IP for testing
//    }
  }
}

# Step 10: Create IAP Tunneling
resource "google_iap_tunnel_instance_iam_binding" "enable_iap" {
  project = var.project
  zone = var.bastion_host_zone
  instance = google_compute_instance.bastion_host.name
  members = [ "serviceAccount:${var.tf_gcp_sa_email}" ]
  role = "roles/iap.tunnelResourceAccessor"
}