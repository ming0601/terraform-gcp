variable "gcp_auth_file" {
  type        = string
  description = "GCP authentication file"
}

variable "app_project" {
  type        = string
  description = "GCP project name"
}
