const express = require('express');
const path = require('path');
const app = express();
const port = 80;
 
app.set('view engine', 'jade');
app.set('views', __dirname);
 
app.get('/', function(req, res, next) {
   res.render('index', { instance: process.env.HOSTNAME });
});
 
app.listen(port, function() {
   console.log('Express server listening on port ' + port);
});
