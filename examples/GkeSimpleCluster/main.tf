terraform {
  required_version = "0.14.7"
}

provider "google" {
  credentials = file(var.gcp_auth_file)
  project     = var.app_project
  region      = "us-central1"
}

# Step 1: Create GKE with 2 nodes
resource "google_container_cluster" "gke-cluster" {
  name               = "node-app-gke-cluster"
  network            = "default"
  location           = "us-central1-b"
  initial_node_count = 2
}


# Step 2: Manage Docker image
    # 2.1: Build Docker image: go to local app folder and use the following syntax to build a Docker image
        # docker build -t node_app .

    # 2.2: Create a tag for the the new image
        # docker tag node_app gcr.io/[PROJECT_ID]/node_app

    # 2.3: Push to Google Container Registry
        # docker push gcr.io/[PROJECT_ID]/node_app


# Step 3: When all the infra is applied by Terraform and the Docker image is pushed in GCR
# Open Cloud Shell or your terminal if CloudSDK is installed and use the following command lines to deploy and scale node_app

    # gcloud container clusters get-credentials node-app-gke-cluster --zone us-central1-b

    # kubectl create deployment node-app --image=gcr.io/[PROJECT_ID]/node_app:latest

    # kubectl scale deployment node-app --replicas=3

    # kubectl autoscale deployment node-app --cpu-percent=80 --min=2 --max=5

    # kubectl expose deployment node-app --name=hello-app-lb --type=LoadBalancer --port 80 --target-port 80

    # kubectl get service


