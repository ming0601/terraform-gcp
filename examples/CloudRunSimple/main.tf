terraform {
  required_version = "0.14.7"
}

provider "google" {
  credentials = file(var.gcp_auth_file)
  project     = var.app_project
  region      = var.gcp_region
}

# Step 1: Pull the httpd image from Docker Hub, tag it and push it to the Google Container Registry
# (Cloud Run can't pull image outside GCR or Artifact Registry)
    # docker pull httpd:alpine
    # docker tag httpd:alpine gcr.io/[PROJECT_ID]/httpd:alpine
    # docker push gcr.io/[PROJECT_ID]/httpd:alpine

# Step 2: Enable the Cloud Run API (it's not enabled on projects by default)
resource "google_project_service" "run_api" {
  service            = "run.googleapis.com"
  disable_on_destroy = true
}

# Step 3: Create the Cloud Run service using httpd (Apache web server) from Docker Hub
resource "google_cloud_run_service" "run_service" {
  location = var.gcp_region
  name     = "playoff-run-app"

  template {
    spec {
      containers {
        image = "gcr.io/${var.app_project}/httpd:alpine"
      }
    }
  }

  // Control the traffic for this revision
  traffic {
    // Indicates how much traffic will be redirected to this revision
    percent = 100
    // Specifies that this traffic config needs to be used for the latest revision
    latest_revision = true
  }
  depends_on = [google_project_service.run_api]
}

# Step 4: Allow unauthenticated users to invoke the service through an HTTP endpoint
# Other possible ways than HTTP: gRPC requests, WebSockets, GCP products like Cloud Scheduler
# (By default, Cloud Run services are private and secured by IAM: Cloud Run Invoker permission needed)
resource "google_cloud_run_service_iam_member" "run_all_members" {
  member   = "allUsers"
  role     = "roles/run.invoker"
  service  = google_cloud_run_service.run_service.name
  location = google_cloud_run_service.run_service.location
}

# Display the Cloud Run service URL
output "run_service_url" {
  value = google_cloud_run_service.run_service.status[0].url
}

# Step 5: Cloud Run listens to port 8080 by default but the httpd listens on port 80
# Presently Terraform provider for changing the container listening port is not implemented even it is in their documentation
# link TF : https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_run_service#ports
# link StackOverFlow : https://stackoverflow.com/questions/60693249/want-to-deploy-a-google-cloud-run-service-via-terraform
# My solution is to update Cloud run services with the right port in Command line
    # gcloud run services update playoff-run-app --port=80 --region=[YOUR_GCP_REGION]
