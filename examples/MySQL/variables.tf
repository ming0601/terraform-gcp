variable "app_project" {
  type = string
  description = "GCP project name"
}

variable "app_name" {
  type = string
  description = "GCP application name"
}

variable "gcp_auth_file" {
  type = string
  description = "GCP authentication file"
}

variable "gcp_region_1" {
  type = string
  description = "GCP region"
}

variable "gcp_zone_1" {
  type = string
  description = "GCP zone"
}