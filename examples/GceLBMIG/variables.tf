variable "gcp_auth_file" {
  type = string
  description = "GCP authentication file"
}

variable "app_project" {
  type = string
  description = "GCP project name"
}

variable "app_name" {
  type = string
  description = "GCP application name"
}

variable "app_domain" {
  type = string
  description = "GCP application domain name"
}

variable "gcp_region_1" {
  type = string
  description = "GCP region"
}

variable "gcp_zone_1" {
  type = string
  description = "GCP zone"
}

variable "private_subnet_cidr_1" {
  type = string
  description = "private subnet CIDR 1"
}

variable "lb_max_replicas" {
  type = string
  description = "Maximum number of VMs for autoscale"
  default = "4"
}

variable "lb_min_replicas" {
  type = string
  description = "Minimum number of VMs for autoscale"
  default = "1"
}

# Number of seconds that the autoscaler should wait before it starts collecting information
variable "lb_cooldown_period" {
  type = string
  description = "The number of seconds that the autoscaler should wait before it starts collecting information from a new instance"
  default = "60"
}