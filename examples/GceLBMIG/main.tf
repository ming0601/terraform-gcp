terraform {
  required_version = "=0.14.7"
}

# Step 1: Providers
provider "google" {
  project = var.app_project
  credentials = file(var.gcp_auth_file)
  region = var.gcp_region_1
  zone = var.gcp_zone_1
}

provider "google-beta" {
  project = var.app_project
  credentials = file(var.gcp_auth_file)
  region = var.gcp_region_1
  zone = var.gcp_zone_1
}

# Step 2: Network
# Step 3: NAT Router
module "vpc_network" {
  source = "../../modules/vpc-network"
  app_name = var.app_name
  app_project = var.app_project
  gcp_region_1 = var.gcp_region_1
  private_subnet_cidr_1 = var.private_subnet_cidr_1
}

# Step 4: Firewall rules
module "network_firewall" {
  source = "../../modules/network-firewall"
  app_name = var.app_name
  network_name = module.vpc_network.vpc_network_name
}

# Step 5: VMs
module "gce_private_mig_template" {
  source = "../../modules/vm-private-mig-template"
  app_name = var.app_name
  vpc_name = module.vpc_network.vpc_network_name
  private_subnet_name = module.vpc_network.vpc_private_subnet_name
  metadata_startup_script = "sudo apt-get update && sudo apt-get install -yq build-essential apache2"
}

# Step 6: LB
module "lb_managed" {
  source = "../../modules/load-balancers/managed"
  app_name = var.app_name
  app_project = var.app_project
  gcp_zone = var.gcp_zone_1
  mig_instance_template_self_link = module.gce_private_mig_template.private_mig_instance_template_self_link
}