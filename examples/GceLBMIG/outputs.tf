output "load-balancer-ip-address" {
  value = module.lb_managed.load-balancer-ip-address
}

output "nat_ip_address" {
  value = module.vpc_network.nat_ip_address
}