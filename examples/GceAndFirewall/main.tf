terraform {
  required_version = "=0.14.7"
}

provider "google" {
  credentials = file("../../../credentials/MFP_GCE_CREDENTIALS.json")
  project = "project"
  region = "us-west1"
  zone = "us-west1-a"
}

resource "google_compute_instance" "instance" {
  machine_type = "f1-micro"
  name = "tf-test-instance"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  metadata_startup_script = <<-EOT
    #!bin/bash
    sudo apt-get update && sudo apt-get install apache2 -y &&
    echo '<!doctype html><html><body><h1>Hello First Terraform Project!</h1></body></html>' | sudo tee /var/www/html/index.html
    EOT

  network_interface {
    network = "default"

    access_config {
      network_tier = "STANDARD"
    }
  }

  scheduling {
    preemptible = true
    automatic_restart = false
  }
  // Apply the firewall rule to allow external IPs to access this instance
  tags = ["http-server"]
}

resource "google_compute_firewall" "default" {
  name = "default-allow-http"
  network = "default"

  allow {
    protocol = "tcp"
    ports = ["80"]
  }

  // Allow traffic from everywhere to instances with an http-server tag
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["http-server"]
}

output "ip" {
  value = google_compute_instance.instance.network_interface[0].access_config[0].nat_ip
}