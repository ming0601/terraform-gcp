output "load-balancer-ip-address" {
  value = module.lb_managed.load-balancer-ip-address
}

output "nat_ip_address" {
  value = module.vpc_network.nat_ip_address
}

# DB outputs
output "db_instance_address" {
  # IP address of the master DB instance
  value = module.my_sql.db_instance_address
}

output "db_instance_name" {
  value = module.my_sql.db_instance_name
}

output "db_instance_username" {
  value = module.my_sql.db_instance_username
}

output "db_instance_generated_password" {
  # The auto generated password if no input password provided
  value = module.my_sql.db_instance_generated_password
}