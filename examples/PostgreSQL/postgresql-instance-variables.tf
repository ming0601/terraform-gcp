# DB instance settings
variable "db_version" {
  description = "The version of the DB. Ex: POSTGRES_9_6 or POSTGRES_11"
  default = "POSTGRES_11"
}

variable "db_tier" {
  description = "The machine tier (1st gen) or type (2nd gen). Ref: https://cloud.google.com/sql/pricing"
  default = "db-f1-micro"
}

variable "db_activation_policy" {
  description = "Specifies when the instance should be active. Options are ALWAYS, NEVER or ON_DEMAND"
  default = "ALWAYS"
}

variable "db_disk_autoresize" {
  description = "2nd gen only. Configuration to increase storage size automatically"
  default = true
}

variable "db_disk_size" {
  description = "2nd gen only. The size of data disk, in GB. Size of running instance cannot be reduced but can be increased"
  default = 10
}

variable "db_disk_type" {
  description = "2nd gen only. The type of data disk: PD_SSD or PD_HDD"
  default = "PD_SSD"
}

variable "db_pricing_plan" {
  description = "1st gen only. Pricing plan for this instance, can be one of PER_USE or PACKAGE"
  default = "PER_USE"
}

variable "db_instance_access_cidr" {
  description = "The IPv4 CIDR to provide access to the DB instance"
  default = "0.0.0.0/0"
}

# DB settings
variable "db_name" {
  description = "The name of the DB to create"
  default = "tf-postgres-db"
}

variable "db_charset" {
  description = "The charset of the default DB"
  default = ""
}

variable "db_collation" {
  description = "The collation for the default DB. Example for MySQL DB: 'utf8_general_ci'"
  default = ""
}

# User settings
variable "db_user_name" {
  description = "The name of the default user"
  default = "dbadmin"
}

variable "db_user_host" {
  description = "The host for the default user"
  default = "%"
}

variable "db_user_password" {
  description = "The password for the default user. If not set, a random one will be generated and available in the generated_user_password output variable"
  default = ""
}