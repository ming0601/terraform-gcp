output "load-balancer-ip-address" {
  value = module.lb_managed.load-balancer-ip-address
}
