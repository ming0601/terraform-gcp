terraform {
  required_version = "0.14.7"
}

# Step 1: Providers
provider "google" {
  credentials = file(var.gcp_auth_file)
  project = var.app_project
  region = var.gcp_region
}

provider "google-beta" {
  credentials = file(var.gcp_auth_file)
  project = var.app_project
  region = var.gcp_region
}

# Step 2: Network
# Step 3: NAT Router
module "vpc_network" {
  source = "../../modules/vpc-network"
  app_name = var.cluster_name
  app_project = var.app_project
  gcp_region_1 = var.gcp_region
  private_subnet_cidr_1 = var.vpc_cidr_block
}


# Step 4: Create Firewall rules
# 4.1 Create a SSH Firewall rule to access Jumphost
resource "google_compute_firewall" "fw_allow_ssh_from_iap_to_instances" {
  name = "${var.cluster_name}-allow-ssh-from-iap"
  network = module.vpc_network.vpc_network_name
  direction = "INGRESS"

  allow {
    protocol = "tcp"
    ports = [ "22" ]
  }

  source_ranges = [ "35.235.240.0/20" ]
  target_tags = [ "ssh-to-jumphost" ]
}

# 4.2 Create a HTTP Firewall rule to access MIG
resource "google_compute_firewall" "allow_http_to_mig" {
  name = "${var.cluster_name}-fw-allow-http"
  network = module.vpc_network.vpc_network_name

  allow {
    protocol = "tcp"
    ports = [ "80" ]
  }

  target_tags = [ "http" ]
}

# Step 5: Create the Jumphost
resource "google_compute_instance" "jumphost" {
  project = var.app_project
  name = "nucleus-jumphost"
  machine_type = "f1-micro"
  zone = var.gcp_zone
  allow_stopping_for_update = true
  tags = [ "ssh-to-jumphost" ]

  metadata_startup_script = "sudo apt-get update && sudo apt-get install -y kubectl"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }
  network_interface {
    network = module.vpc_network.vpc_network_self_link
    subnetwork = module.vpc_network.vpc_private_subnet_self_link
  }
}

# Step 6: Create IAP Tunneling
resource "google_iap_tunnel_instance_iam_binding" "enable_iap" {
  project = var.app_project
  zone = var.gcp_zone
  instance = google_compute_instance.jumphost.name
  members = [ "serviceAccount:${var.tf_gcp_sa_email}" ]
  role = "roles/iap.tunnelResourceAccessor"
}

# Step 7: GKE master cluster
module "gke_cluster" {
  source = "../../modules/gke-cluster"
  depends_on = [ module.vpc_network ]

  name = var.cluster_name
  project = var.app_project
  location = var.gcp_region

  network = module.vpc_network.vpc_network_self_link
  subnetwork = module.vpc_network.vpc_private_subnet_self_link

  master_ipv4_cidr_block = var.master_ipv4_cidr_block

  enable_private_nodes = true

  disable_public_endpoint = true

  master_authorized_networks_config = [
    {
      cidr_blocks = [
        {
          cidr_block = module.vpc_network.vpc_private_subnet_ip_cidr_range
          display_name = "${module.vpc_network.vpc_private_subnet_name}-IP-Range"
        }
      ]
    }
  ]

  cluster_secondary_range_name = null

  enable_vertical_pod_autoscaling = "true"

  resource_labels = {
    environment = "testing"
  }
}

# Step 8: Create a GKE service account
module "gke_service_account" {
  source = "../../modules/gke-service-account"
  name = var.cluster_service_account_name
  project = var.app_project
  description = var.cluster_service_account_description
}

# Step 9: Create private node pools
resource "google_container_node_pool" "node_pool" {
  provider = google-beta
  name = "private-pool"
  project = var.app_project
  location = var.gcp_region
  cluster = module.gke_cluster.name

  initial_node_count = 3

  management {
    auto_repair = true
    auto_upgrade = true
  }

  node_config {
    image_type = "COS"
    machine_type = "n1-standard-1"

    labels = {
      private-pool-nucleus = "true"
    }

    tags = [ module.vpc_network.vpc_network_name, "private-pool-nucleus" ]

    disk_size_gb = 30
    disk_type = "pd-standard"
    preemptible = false

    service_account = module.gke_service_account.email
    oauth_scopes = [ "https://www.googleapis.com/auth/cloud-platform" ]
  }

  lifecycle {
    ignore_changes = [ initial_node_count ]
  }

  timeouts {
    create = "30m"
    update = "30m"
    delete = "30m"
  }
}

# Step 10: MIG VMs
module "gce_private_mig_template" {
  source = "../../modules/vm-private-mig-template"
  app_name = var.cluster_name
  vpc_name = module.vpc_network.vpc_network_name
  private_subnet_name = module.vpc_network.vpc_private_subnet_name

  metadata_startup_script = <<-EOT
    #!/bin/bash
    sudo apt-get update && sudo apt-get install -y nginx
    sudo service nginx start
    sed -i -- 's/nginx/Google Cloud Platform - '"\$HOSTNAME"'/' /var/www/html/index.nginx-debian.html
  EOT
}

# Step 11: LB with health_check, private VMs, autoscaler, backend_service, url_map, target_http_proxy & global_forwarding_rule
module "lb_managed" {
  source = "../../modules/load-balancers/managed"
  app_name = var.cluster_name
  app_project = var.app_project
  gcp_zone = var.gcp_zone
  mig_instance_template_self_link = module.gce_private_mig_template.private_mig_instance_template_self_link
}
