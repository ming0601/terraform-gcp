variable "gcp_auth_file" {
  type = string
  description = "GCP authentication file"
}

variable "app_project" {
  type = string
  description = "GCP project name"
}

variable "gcp_region" {
  type = string
  description = "GCP region"
}

variable "gcp_zone" {
  type = string
  description = "GCP zone"
}

variable "tf_gcp_sa_email" {
  description = "GCP Service Account Email, used for IAP Tunneling here"
  type = string
}

variable "cluster_name" {
  description = "The name of the Kubernetes cluster."
  type = string
  default = "nucleus-private-cluster"
}

variable "cluster_service_account_name" {
  description = "The name of the custom service account used for the GKE cluster. This parameter is limited to a maximum of 28 characters."
  type = string
  default = "nucleus-private-cluster-sa"
}

variable "cluster_service_account_description" {
  description = "A description of the custom service account used for the GKE cluster."
  type = string
  default = "nucleus GKE Cluster Service Account managed by Terraform"
}

variable "master_ipv4_cidr_block" {
  description = "The IP range in CIDR notation (size must be /28) to use for the hosted master network. This range will be used for assigning internal IP addresses to the master or set of masters, as well as the ILB VIP. This range must not overlap with any other ranges in use within the cluster's network."
  type = string
  default = "10.5.0.0/28"
}

variable "vpc_cidr_block" {
  description = "The IP address range of the VPC in CIDR notation. A prefix of /16 is recommended. Do not use a prefix higher than /27."
  type = string
  default = "10.3.0.0/16"
}
