variable "gcp_auth_file" {
  type        = string
  description = "GCP authentication file"
}

variable "app_project" {
  type        = string
  description = "GCP project name"
}

variable "app_name" {
  type        = string
  description = "GCP application name"
  default     = "nucleus-app"
}

variable "gcp_region" {
  type        = string
  description = "GCP region"
  default     = "us-central1"
}

variable "gcp_zone" {
  type        = string
  description = "GCP zone"
  default     = "us-central1-b"
}
