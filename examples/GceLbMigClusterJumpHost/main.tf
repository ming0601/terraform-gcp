terraform {
  required_version = "0.14.7"
}

provider "google" {
  credentials = file(var.gcp_auth_file)
  project     = var.app_project
  region      = var.gcp_region
}

# Step 1: Create Jumphost VM
resource "google_compute_instance" "jumphost" {
  project                   = var.app_project
  name                      = "nucleus-jumphost"
  machine_type              = "f1-micro"
  zone                      = var.gcp_zone
  allow_stopping_for_update = true

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }
  network_interface {
    network = "default"
  }
}

# Step 2: Create GKE with 3 nodes
resource "google_container_cluster" "gke-cluster" {
  name               = "nucleus-gke-cluster"
  network            = "default"
  location           = var.gcp_zone
  initial_node_count = 3
}

# Step 3: Create HTTP Firewall rules for ingress
resource "google_compute_firewall" "allow-http" {
  name        = "${var.app_name}-fw-allow-http"
  network     = "default"
  target_tags = ["http"]

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }
}

# Step 4: Create instance template
resource "google_compute_instance_template" "web_server" {
  name                 = "${var.app_name}-web-server-template"
  description          = "This template is used to create an Nginx web server"
  instance_description = "Web server running Nginx"
  can_ip_forward       = false
  machine_type         = "f1-micro"
  tags                 = ["http"]

  scheduling {
    automatic_restart   = true
    on_host_maintenance = "MIGRATE"
  }

  disk {
    source_image = "debian-cloud/debian-9"
    auto_delete  = true
    boot         = true
  }

  network_interface {
    network = "default"
    access_config {}
  }

  lifecycle {
    create_before_destroy = true
  }

  metadata_startup_script = <<-EOT
    #!/bin/bash
    sudo apt-get update && sudo apt-get install -y nginx
    sudo service nginx start
    sed -i -- 's/nginx/Google Cloud Platform - '"\$HOSTNAME"'/' /var/www/html/index.nginx-debian.html
  EOT

}

# Step 5: Create Health chek for VM instances
resource "google_compute_health_check" "health_check" {
  name               = "${var.app_name}-health-check"
  timeout_sec        = 1
  check_interval_sec = 1

  http_health_check {
    port = 80
  }
}

# Step 5: Create MIG with instance templates created on Step 4
resource "google_compute_instance_group_manager" "web_private_group" {
  name               = "${var.app_name}-vm-group"
  project            = var.app_project
  base_instance_name = "${var.app_name}-web"
  zone               = var.gcp_zone

  version {
    instance_template = google_compute_instance_template.web_server.self_link
  }

  named_port {
    name = "http"
    port = 80
  }
}

# Step 6: Create MIG autoscaler
# Automatically scale VM instances in MIG according to autoscaling policy
resource "google_compute_autoscaler" "autoscaler" {
  name    = "${var.app_name}-autoscaler"
  project = var.app_project
  zone    = var.gcp_zone
  target  = google_compute_instance_group_manager.web_private_group.self_link

  autoscaling_policy {
    max_replicas    = "4"
    min_replicas    = "2"
    cooldown_period = "60"

    cpu_utilization {
      target = 0.8
    }
  }
}

# Step 7: Create Backend service and associate it to the MIG
resource "google_compute_backend_service" "backend_service" {
  name                  = "${var.app_name}-backend-service"
  project               = var.app_project
  port_name             = "http"
  protocol              = "HTTP"
  load_balancing_scheme = "EXTERNAL"
  health_checks         = [google_compute_health_check.health_check.self_link]

  backend {
    group                 = google_compute_instance_group_manager.web_private_group.instance_group
    balancing_mode        = "RATE"
    max_rate_per_instance = 100
  }
}

# Step 8: Create URL map and associate it to the backend service
resource "google_compute_url_map" "url_map" {
  name            = "${var.app_name}-load-balancer"
  project         = var.app_project
  default_service = google_compute_backend_service.backend_service.self_link
}

# Step 9: Create HTTP Proxy to associate with the URL map
resource "google_compute_target_http_proxy" "target_http_proxy" {
  name    = "${var.app_name}-proxy"
  project = var.app_project
  url_map = google_compute_url_map.url_map.self_link
}

# Step 10: Create global forwarding rule targeting the previous HTTP Proxy on port 80
resource "google_compute_global_forwarding_rule" "global_forwarding_rule" {
  name       = "${var.app_name}-global-forwarding-rule"
  project    = var.app_project
  target     = google_compute_target_http_proxy.target_http_proxy.self_link
  port_range = "80"
}

# Step 11: When all the infra is applied by Terraform
# Open Cloud Shell or your terminal if CloudSDK is installed and use the following command lines to deploy hello-world

# gcloud container clusters get-credentials nucleus-gke-cluster --zone us-central1-b

# kubectl create deployment hello-app --image=registry.hub.docker.com/library/hello-world:latest

# kubectl expose deployment hello-app --type=LoadBalancer --port 8080

# kubectl get service
