output "private_vm_instance_self_link" {
  value = google_compute_instance.web_private.self_link
}

output "web-name" {
  value = google_compute_instance.web_private.name
}

output "web-internal-ip" {
  value = google_compute_instance.web_private.network_interface.0.network_ip
}