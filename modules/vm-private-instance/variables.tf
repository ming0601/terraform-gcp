variable "app_name" {
  type = string
  description = "GCP application name"
}

variable "gcp_zone" {
  type = string
  description = "GCP application zone"
}

variable "app_hostname" {
  type = string
  description = "GCP application hostname (app_name.app_domain)"
}

variable "vpc_name" {
  type = string
  description = "GCP VPC name"
}

variable "private_subnet_name" {
  type = string
  description = "GCP private subnet name"
}

variable "metadata_startup_script" {
  type = string
  description = "VM metadata startup script"
}