resource "google_compute_instance" "web_private" {
  name = var.app_name
  machine_type = "f1-micro"
  zone = var.gcp_zone
  hostname = var.app_hostname
  tags = ["ssh","http"]

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1804-lts"
    }
  }

  metadata_startup_script = var.metadata_startup_script

  network_interface {
    network = var.vpc_name
    subnetwork = var.private_subnet_name
  }

  # Do not include the line access_config { } to create VM with private IP only
}