# Add global_address for HTTP(S) LB and VPC_PEERING to connect multiple VPCs
resource "google_compute_global_address" "private_ip_address" {
  provider = google-beta
  name = "${var.app_name}-private-ip-address"
  purpose = "VPC_PEERING"
  address_type = "INTERNAL"
  prefix_length = 16
  network = var.vpc_network_id
}

# Manage a private VPC connection with a GCP service provider
resource "google_service_networking_connection" "private_vpc_connection" {
  network = var.vpc_network_self_link
  service = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [ google_compute_global_address.private_ip_address.name ]
}