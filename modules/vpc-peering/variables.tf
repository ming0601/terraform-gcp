variable "app_name" {
  description = "GCP application name"
  type = string
}

variable "vpc_network_id" {
  description = "GCP VPC network id for creating google compute global address (private ip address)"
  type = string
}

variable "vpc_network_self_link" {
  description = "GCP VPC network self link for creating google service networking connection (private vpc connection)"
  type = string
}