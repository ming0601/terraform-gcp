variable "app_project" {
  type = string
  description = "GCP project name"
}

variable "app_name" {
  description = "GCP application name"
  type = string
}

variable "gcp_region_1" {
  type = string
  description = "GCP region"
}

variable "private_subnet_cidr_1" {
  description = "private subnet CIDR 1"
  type = string
}