# VPC Network Module

The VPC Network module creates a Virtual Private Cloud (VPC) network on Google Cloud Platform (GCP) following best practices.

Other example: https://github.com/gruntwork-io/terraform-google-network/tree/master/modules/vpc-network