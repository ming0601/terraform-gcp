output "nat_ip_address" {
  value = google_compute_address.nat-ip.address
}

output "vpc_network_name" {
  value = google_compute_network.vpc.name
}

output "vpc_network_id" {
  value = google_compute_network.vpc.id
}

output "vpc_network_self_link" {
  value = google_compute_network.vpc.self_link
}

output "vpc_private_subnet_name" {
  value = google_compute_subnetwork.private_subnet_1.name
}

output "vpc_private_subnet_self_link" {
  value = google_compute_subnetwork.private_subnet_1.self_link
}

output "vpc_private_subnet_ip_cidr_range" {
  value = google_compute_subnetwork.private_subnet_1.ip_cidr_range
}