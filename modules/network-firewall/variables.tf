variable "app_name" {
  description = "GCP application name"
  type = string
}

variable "network_name" {
  description = "name of the VPC network to apply firewall rules to"
  type = string
}