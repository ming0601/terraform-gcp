resource "google_compute_firewall" "allow-http" {
  name = "${var.app_name}-fw-allow-http"
  network = var.network_name
  target_tags = ["http"]

  allow {
    protocol = "tcp"
    ports = ["80"]
  }
}

resource "google_compute_firewall" "allow-https" {
  name = "${var.app_name}-fw-allow-https"
  network = var.network_name
  target_tags = ["https"]

  allow {
    protocol = "tcp"
    ports = ["443"]
  }
}

resource "google_compute_firewall" "allow-ssh" {
  name = "${var.app_name}-fw-allow-ssh"
  network = var.network_name
  target_tags = ["ssh"]

  allow {
    protocol = "tcp"
    ports = ["22"]
  }
}

resource "google_compute_firewall" "allow-rdp" {
  name = "${var.app_name}-fw-allow-rdp"
  network = var.network_name
  target_tags = ["rdp"]

  allow {
    protocol = "tcp"
    ports = ["3389"]
  }
}