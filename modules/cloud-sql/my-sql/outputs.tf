# DB outputs
output "db_instance_address" {
  # IP address of the master DB instance
  value = google_sql_database_instance.my_sql_instance.ip_address.0.ip_address
}

output "db_instance_name" {
  value = google_sql_database_instance.my_sql_instance.name
}

output "db_instance_username" {
  value = var.db_user_name
}

output "db_instance_generated_password" {
  # The auto generated password if no input password provided
  value = random_id.user_password.hex
}