resource "google_compute_instance_template" "web_server" {
  name = "${var.app_name}-web-server-template"
  description = "This template is used to create Apache web server"
  instance_description = "Web server running Apache"
  can_ip_forward = false
  machine_type = "f1-micro"
  tags = ["ssh","http"]

  scheduling {
    automatic_restart = true
    on_host_maintenance = "MIGRATE"
  }

  disk {
    source_image = "ubuntu-os-cloud/ubuntu-1804-lts"
    auto_delete = true
    boot = true
  }

  network_interface {
    network = var.vpc_name
    subnetwork = var.private_subnet_name
  }

  lifecycle {
    create_before_destroy = true
  }

  metadata_startup_script = var.metadata_startup_script

  # Do not include the line access_config { } to create VM with private IP only
}