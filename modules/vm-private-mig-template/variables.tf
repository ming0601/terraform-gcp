variable "app_name" {
  type = string
  description = "GCP application name"
}

variable "vpc_name" {
  type = string
  description = "GCP VPC name"
}

variable "private_subnet_name" {
  type = string
  description = "GCP private subnet name"
}

variable "metadata_startup_script" {
  type = string
  description = "VM metadata startup script"
}