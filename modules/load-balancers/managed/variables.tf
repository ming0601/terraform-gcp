variable "app_project" {
  type = string
  description = "GCP project name"
}

variable "app_name" {
  type = string
  description = "GCP application name"
}

variable "gcp_zone" {
  type = string
  description = "GCP zone"
}

variable "mig_instance_template_self_link" {
  type = string
  description = "Instance template self link of the GCP managed instance group (MIG)"
}

variable "lb_max_replicas" {
  type = string
  description = "Maximum number of VMs for autoscale"
  default = "4"
}

variable "lb_min_replicas" {
  type = string
  description = "Minimum number of VMs for autoscale"
  default = "1"
}

# Number of seconds that the autoscaler should wait before it starts collecting information
variable "lb_cooldown_period" {
  type = string
  description = "The number of seconds that the autoscaler should wait before it starts collecting information from a new instance"
  default = "60"
}