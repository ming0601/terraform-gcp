variable "app_project" {
  type = string
  description = "GCP project name"
}

variable "app_name" {
  type = string
  description = "GCP application name"
}

variable "gcp_zone" {
  type = string
  description = "GCP zone"
}

variable "mig_instance_self_links_list" {
  type = set(string)
  description = "List of instance template self links of the GCP unmanaged instance group (UIG)"
}