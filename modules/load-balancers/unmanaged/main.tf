# Determine whether instances are responsive and able to do work
resource "google_compute_health_check" "health_check" {
  name = "${var.app_name}-healthcheck"
  timeout_sec = 1
  check_interval_sec = 1

  http_health_check {
    port = 80
  }
}

# Creates a group of dissimilar VMs
resource "google_compute_instance_group" "web_private_group" {
  name = "${var.app_name}-vm-group"
  description = "Web servers instance group"
  zone = var.gcp_zone
  instances = var.mig_instance_self_links_list

  named_port {
    name = "http"
    port = 80
  }
}

# Defines a group of VM that will serve traffic for load balancing
# Needs the previous 2 resources
resource "google_compute_backend_service" "backend_service" {
  name = "${var.app_name}-backend-servce"
  project = var.app_project
  port_name = "http"
  protocol = "HTTP"
  health_checks = [ google_compute_health_check.health_check.self_link ]

  backend {
    group = google_compute_instance_group.web_private_group.self_link
    balancing_mode = "RATE"
    max_rate_per_instance = 100
  }
}

# Used to route requests to a backend service based on rules that you define for the host and path of an incoming URL
# Needs the previous resource
resource "google_compute_url_map" "url_map" {
  name = "${var.app_name}-load-balancer"
  project = var.app_project
  default_service = google_compute_backend_service.backend_service.self_link
}

# Used one or more global forwarding rule to route incoming HTTP requests to a URL map
# Needs the previous resource
resource "google_compute_target_http_proxy" "target_http_proxy" {
  name = "${var.app_name}-proxy"
  project = var.app_project
  url_map = google_compute_url_map.url_map.self_link
}

# Used to forward traffic to the correct load balancer for HTTP load balancing
# Needs the previous resource
resource "google_compute_global_forwarding_rule" "global_forwarding_rule" {
  name = "${var.app_name}-global-forwarding-rule"
  project = var.app_project
  target = google_compute_target_http_proxy.target_http_proxy.self_link
  port_range = "80"
}

